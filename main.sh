#!/bin/bash
if [[ ! -d "photos" ]]
then
    mkdir photos
    echo Veuillez mettre des photos à trier dans le répertoire qui viens d\' être créer !
    exit 0
fi
avec_rep() {
    cd photos

    # Déclaration du tableau pour les formats pris en charges
    declare -a formats=( "*.ai" "*.es" "*.pdf" "*.png" "*.psd" "*.jpg" "*.jpeg" "*.gif" "*.tif" "*.svg" "*.raw")

    for i in $(ls ${formats[@]})
    do

        ans=$(date -r $i '+%Y')
        mois=$(date -r $i '+%B')
        nom=$(date -r $i '+%Y-%m-%d-%H%M%S')
        
        # Création des dossiers pour le tri
        if [ $param = 'a' ]
        then
            mkdir annee-$ans  
        elif [ $param = 'am' ]
        then
            mkdir annee-$ans
            cd annee-$ans
            mkdir mois-$mois
            cd ..
        fi

        # Vérification des extensions
        if [ ${i##*.} = 'ai' ]
        then
            if [[ $param = 'a' ]]
            then
                mv $i annee-$ans/$nom.ai
            elif [[ $param = 'am' ]]
            then
                mv $i annee-$ans/mois-$mois/$nom.ai
            fi

        elif [ ${i##*.} = 'es' ]
        then
            if [[ $param = 'a' ]]
            then
                mv $i annee-$ans/$nom.es
            elif [[ $param = 'am' ]]
            then
                mv $i annee-$ans/mois-$mois/$nom.es
            fi

        elif [ ${i##*.} = 'pdf' ]
        then
            if [[ $param = 'a' ]]
            then
                mv $i annee-$ans/$nom.pdf
            elif [[ $param = 'am' ]]
            then
                mv $i annee-$ans/mois-$mois/$nom.pdf
            fi

        elif [ ${i##*.} = 'png' ]
        then
            if [[ $param = 'a' ]]
            then
                mv $i annee-$ans/$nom.png
            elif [[ $param = 'am' ]]
            then
                mv $i annee-$ans/mois-$mois/$nom.png
            fi

        elif [ ${i##*.} = 'psd' ]
        then
            if [[ $param = 'a' ]]
            then
                mv $i annee-$ans/$nom.psd
            elif [[ $param = 'am' ]]
            then
                mv $i annee-$ans/mois-$mois/$nom.psd
            fi
        
        elif [ ${i##*.} = 'jpg' ]
        then
            if [[ $param = 'a' ]]
            then
                mv $i annee-$ans/$nom.jpg
            elif [[ $param = 'am' ]]
            then
                mv $i annee-$ans/mois-$mois/$nom.jpg
            fi

        elif [ ${i##*.} = 'jpeg' ]
        then
            if [[ $param = 'a' ]]
            then
                mv $i annee-$ans/$nom.jpeg
            elif [[ $param = 'am' ]]
            then
                mv $i annee-$ans/mois-$mois/$nom.jpeg
            fi

        elif [ ${i##*.} = 'gif' ]
        then
            if [[ $param = 'a' ]]
            then
                mv $i annee-$ans/$nom.gif
            elif [[ $param = 'am' ]]
            then
                mv $i annee-$ans/mois-$mois/$nom.gif
            fi

        elif [ ${i##*.} = 'tif' ]
        then
            if [[ $param = 'a' ]]
            then
                mv $i annee-$ans/$nom.tif
            elif [[ $param = 'am' ]]
            then
                mv $i annee-$ans/mois-$mois/$nom.tif
            fi

        elif [ ${i##*.} = 'svg' ]
        then
            if [[ $param = 'a' ]]
            then
                mv $i annee-$ans/$nom.svg
            elif [[ $param = 'am' ]]
            then
                mv $i annee-$ans/mois-$mois/$nom.svg
            fi

        elif [ ${i##*.} = 'raw' ]
        then
            if [[ $param = 'a' ]]
            then
                mv $i annee-$ans/$nom.raw
            elif [[ $param = 'am' ]]
            then
                mv $i annee-$ans/mois-$mois/$nom.raw
            fi

        fi
    done

    # Retour à la racine du script
    cd ..
    echo Tri terminé !
}

# Fonction par défaut, sans arguments
sans_rep() {
    cd photos

    for i in $(ls *.jpg)
    do
        nom=$(date -r $i '+%Y-%m-%d-%H%M%S')

        if [ ${i##*.} = 'jpg' ]
        then
            mv $i $nom.jpg
        elif [ ${i##*.} = 'png' ]
        then
            mv $i $nom.png
        elif [ ${i##*.} = 'jpeg' ]
        then
            mv $i $nom.jpeg
        elif [ ${i##*.} = 'raw' ]
        then
            mv $i $nom.raw
        fi
    done

    # Retour à la racine du script
    cd ..
    echo Tri terminé !
}

# Check des arguments
if [[ $1 == rep ]]
then
    if [[ $2 == a ]]
    then
        echo OK - A
        param=a
    elif [[ $2 == am ]]
    then
        echo OK - AM
        param=am
    fi

    avec_rep
else
    sans_rep
fi