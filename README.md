# Tri photos automatique

Algorithme de tri de photos automatique

## Usage:
Pour utiliser cet algorithme écrit en BASH, il vous suffit de le mettre à la 
racine d'un dossier quelconque, et, de préférence créer un dossier "photos" dans 
lequel vous allez mettre toutes vos photos à trier.
L'lgorithme s'occupera de les trier par date et heure, sous la forrme 'YYYY-MM-JJ-HHMMSS.jpg'

> Seul le format '.jpg' est pris en charge dans cette première version.
Une verion 2 arrivera avec plus de format de fichier pris en charge et 
certainement plusieurs mise en forme de reformatage de nom
(Pour ne pas seulement se limiter à 'YYYY-MM-JJ-HHMMSS.jpg' ...)

Pour lancer le script, vous n'avez qu'a vous rendre dans le dossier concerner 
avec votre Terminal,
Et de taper ./main.sh

### Commandes:
- ```./main.sh rep am```
- ```./main.sh rep a```
- ```./main.sh rep```
- ```./main.sh```

> 'rep' si vous souhaitez créer des dossier

> sans 'rep' si vous souhaitez juste trier en vrac vos photos

> 'am' pour trier par année et par mois

> 'a' pour trier uniquement par année

+ Des modifications sont prévus pour encore optimiser les temps d'exécutions
 et pour apporter encore plus de fonctions:
- Pour trier par mois et années dans le même répertoire sous la forme 'Mois-AAAA'
- Pour choisir le formatage du nom de votre nouveau fichier :
    - YYYY-MM-JJ-HHMMSS.jpg
    - YYYY-MM-JJ.jpg
    - JJ-MM-AAAA
    - JJ-B-AAAA